package com.siriphonpha.afinal

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.activityViewModels
import androidx.fragment.app.viewModels
import androidx.navigation.fragment.findNavController
import com.siriphonpha.afinal.data.FruitsViewModel
import com.siriphonpha.afinal.databinding.FragmentFruitsBinding

class FruitsFragment : Fragment() {

    //เพิ่มbinding เข้าไป เพื่อสร้างตัวเชื่อม
    private var _binding: FragmentFruitsBinding? = null
    private val Fragmentbinding get() = _binding
    //

    //เพิ่มbinding เข้าไป เพื่อสร้างตัวเชื่อม
    private val viewModel: FruitsViewModel by activityViewModels()
    private lateinit var binding: FragmentFruitsBinding
    //

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        arguments?.let {

        }
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        binding = DataBindingUtil.inflate(inflater, R.layout.fragment_fruits, container, false)
        _binding = FragmentFruitsBinding.inflate(inflater, container, false)
        return binding?.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        binding.fruitViewModel = viewModel
        binding.maxNoOfWords = MAX_NO_OF_WORDS
        binding.lifecycleOwner = viewLifecycleOwner

        binding.btnSubmit.setOnClickListener { onSubmitWord() }
        binding.btnSkip.setOnClickListener { onSkipWord() }
    }

    private fun onSkipWord() {
        if (viewModel.nextWord()) {
            setErrorTextField(false)
        } else {
            showFinalScore()
        }
    }


    private fun onSubmitWord() {
        val playerWord = binding.textInputEditText.text.toString()
        if (viewModel.isUserWordCorrect(playerWord)) {
            setErrorTextField(false)
            if (viewModel.nextWord()) {
            } else {
                showFinalScore()
            }
        } else {
            setErrorTextField(true)
        }

    }

    private fun setErrorTextField(error: Boolean) {
        if (error) {
            binding.textField.isErrorEnabled = true
            binding.textField.error = getString(R.string.tv_newAgain)
        } else {
            binding.textField.isErrorEnabled = false
            binding.textInputEditText.text = null
        }
    }

    private fun showFinalScore() {
        findNavController().navigate(R.id.action_fruitsFragment_to_scoreFruitFragment)
    }

    override fun onDestroyView() {
        _binding = null
        super.onDestroyView()
    }


}