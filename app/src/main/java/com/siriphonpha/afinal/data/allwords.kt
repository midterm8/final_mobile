package com.siriphonpha.afinal


const val MAX_NO_OF_WORDS = 15
const val SCORE_INCREASE = 1


val animalWordsList: List<String> =
    listOf("Bird",
        "Duck",
        "Falcon",
        "Flamingo",
        "Owl",
        "Parrot",
        "Peacock",
        "Penguin",
        "Swan",
        "Hamster",
        "Dragon",
        "Pig",
        "Bat",
        "Unicorn",
        "Ant",
        "Bee",
        "Butterfly",
        "Chicken",
        "Cow",
        "Lamb",
        )


val fruitWordsList: List<String> =
    listOf(
        "Blueberry",
        "Cherry",
        "Grapes",
        "Raspberry",
        "Strawberry",
        "Apple",
        "Avocado",
        "Banana",
        "Coconut",
        "Durian",
        "Mango",
        "Melon",
        "Orange",
        "Papaya",
        "Pineapple",
        "Pomegranate",
        "Peach",
        "Pear",
        "Kiwi",
    )
