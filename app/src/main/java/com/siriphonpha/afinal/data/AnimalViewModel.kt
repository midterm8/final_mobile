package com.siriphonpha.afinal.data

import android.util.Log
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.Transformations
import androidx.lifecycle.ViewModel
import com.siriphonpha.afinal.MAX_NO_OF_WORDS
import com.siriphonpha.afinal.SCORE_INCREASE
import com.siriphonpha.afinal.animalWordsList


class AnimalViewModel: ViewModel() {

    private var _score = MutableLiveData<Int>(0)
    val score: MutableLiveData<Int>
        get() = _score


    private var _currentWordCount = MutableLiveData<Int>(0)
    val currentWordCount: MutableLiveData<Int>
        get() = _currentWordCount

    private var _currentScrambledWord: MutableLiveData<String> = MutableLiveData<String>()
    val currentScrambledWord: LiveData<String>
        get() = _currentScrambledWord


    val currentScrambleWordWithBracket = Transformations.map(_currentScrambledWord) {
        "(${_currentScrambledWord.value})"
    }


    private var animalViewModel: MutableList<String> = mutableListOf()
    private lateinit var currentWord: String
    init {
        Log.d("AnimalsFragment", "AnimalViewModel created!")
        getNextWord()
    }

    /*
* Updates currentWord and currentScrambledWord with the next word.
*/
    private fun getNextWord() {
        currentWord = animalWordsList.random()
        val tempWord = currentWord.toCharArray()
        tempWord.shuffle()

        while (String(tempWord).equals(currentWord, false)) {
            tempWord.shuffle()
        }
        if (animalViewModel.contains(currentWord)) {
            getNextWord()
        } else {
            _currentScrambledWord.value = String(tempWord)
            _currentWordCount.value = _currentWordCount.value?.plus(1)
            animalViewModel.add(currentWord)
        }
    }
    private fun increaseScore() {
        _score.value = _score.value?.plus(SCORE_INCREASE)
    }

    fun nextWord(): Boolean {
        return if (currentWordCount.value!! < MAX_NO_OF_WORDS) {
            getNextWord()
            true
        } else false
    }
    fun isUserWordCorrect(playerWord: String): Boolean {
        if (playerWord.equals(currentWord, true)) {
            increaseScore()
            return true
        }
        return false
    }

    fun reinitializeData() {
        _score.value = 0
        _currentWordCount.value = 0
        animalViewModel.clear()
        getNextWord()
    }
}