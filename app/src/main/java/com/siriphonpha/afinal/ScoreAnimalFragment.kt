package com.siriphonpha.afinal

import android.annotation.SuppressLint
import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.activityViewModels
import androidx.fragment.app.viewModels
import androidx.navigation.findNavController

import com.siriphonpha.afinal.data.AnimalViewModel

import com.siriphonpha.afinal.databinding.FragmentScoreAnimalBinding

class ScoreAnimalFragment : Fragment() {

    //เพิ่มbinding เข้าไป หน้า score
    private var _binding: FragmentScoreAnimalBinding? = null
    private val Fragmentbinding get() = _binding
    //

    //เพิ่มbinding เข้าไป เพื่อสร้างตัวเชื่อม
    private val viewModel: AnimalViewModel by activityViewModels()
    private lateinit var binding: FragmentScoreAnimalBinding
    //

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        arguments?.let {

        }
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        binding =
            DataBindingUtil.inflate(inflater, R.layout.fragment_score_animal, container, false)
        _binding = FragmentScoreAnimalBinding.inflate(inflater, container, false)
        return binding.root
    }

    @SuppressLint("StringFormatMatches")
    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        binding.animalViewModel = viewModel
        binding.lifecycleOwner = viewLifecycleOwner


        binding?.btnAgain?.setOnClickListener {
            val action = ScoreAnimalFragmentDirections.actionScoreAnimalFragmentToMainHomeFragment()
            view.findNavController().navigate(action)
            viewModel.reinitializeData()
        }
    }

    override fun onDestroyView() {
        _binding = null
        super.onDestroyView()
    }
}