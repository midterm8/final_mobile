package com.siriphonpha.afinal

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.navigation.findNavController
import com.siriphonpha.afinal.databinding.FragmentFruitsBinding
import com.siriphonpha.afinal.databinding.FragmentMainHomeBinding

class MainHomeFragment : Fragment() {
    //เพิ่มbinding เข้าไป เพื่อสร้างตัวเชื่อม
    private var _binding: FragmentMainHomeBinding? = null
    private val binding get() = _binding
    //

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        arguments?.let {

        }
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        _binding = FragmentMainHomeBinding.inflate(inflater, container, false)
        return binding?.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        //เพิ่ม action button
        binding?.btnAnimals?.setOnClickListener {
            val action = MainHomeFragmentDirections.actionMainHomeFragmentToAnimalsFragment()
            view.findNavController().navigate(action)
        }
        binding?.btnFruits?.setOnClickListener {
            val action = MainHomeFragmentDirections.actionMainHomeFragmentToFruitsFragment()
            view.findNavController().navigate(action)
        }
        //ถึงตรงนี้
    }

    override fun onDestroyView() {
        _binding = null
        super.onDestroyView()
    }

}