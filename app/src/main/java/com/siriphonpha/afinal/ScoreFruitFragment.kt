package com.siriphonpha.afinal

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.activityViewModels
import androidx.navigation.findNavController
import com.siriphonpha.afinal.data.AnimalViewModel
import com.siriphonpha.afinal.data.FruitsViewModel
import com.siriphonpha.afinal.databinding.FragmentScoreAnimalBinding
import com.siriphonpha.afinal.databinding.FragmentScoreFruitBinding

class ScoreFruitFragment : Fragment() {

    //เพิ่มbinding เข้าไป เพื่อสร้างตัวเชื่อม
    private var _binding: FragmentScoreFruitBinding? = null
    private val Fragmentbinding get() = _binding
    //

    //เพิ่มbinding เข้าไป เพื่อสร้างตัวเชื่อม
    private val viewModel: FruitsViewModel by activityViewModels()
    private lateinit var binding: FragmentScoreFruitBinding
    //

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        arguments?.let {

        }
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        binding =
            DataBindingUtil.inflate(inflater, R.layout.fragment_score_fruit, container, false)
        _binding = FragmentScoreFruitBinding.inflate(inflater, container, false)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        binding.fruitViewModel = viewModel
        binding.lifecycleOwner = viewLifecycleOwner


        binding?.btnAgain?.setOnClickListener {
            val action = ScoreFruitFragmentDirections.actionScoreFruitFragmentToMainHomeFragment()
            view.findNavController().navigate(action)
            viewModel.reinitializeData()
        }
    }

    override fun onDestroyView() {
        _binding = null
        super.onDestroyView()
    }

}